var app = new Vue({
  el: '#app',
  data: {
    day: 1,
    page: "departments",
    waiting: true,
    sel_project: -1,
    modal_department: -1,
    order_task: 0,
    order_project: -1,
    victory: 50,
    news: [{ origin: 'Отдел 1', text: 'Прогресс по прототипу 1...', color: "primary" }, { origin: 'Отдел 2', text: 'Прогресс по прототипу 2...', color: "warning" }],
    tasks: [{ name: 'Разведчик', details: [{ param: "скорость", value: 1, text: 'Высокая скорость' }, { param: "грузоподъемность", value: 15, text: 'Грузоподъемность не ниже 15' }] }, { name: 'Разведчик', details: [{ param: "скорость", value: 1, text: 'Высокая скорость' }, { param: "грузоподъемность", value: 15, text: 'Грузоподъемность не ниже 15' }] }],
    production: [{ project: 0, efficiency: 23, efficiency_rate: 70, cost_rate: 30 }, { project: -1, efficiency: 0, efficiency_rate: 0, cost_rate: 0 }],
    enemy_production: [{ name: "F-16", efficiency: 23, cost: 70 }, { name: "", efficiency: 0, cost: 1 }],
    // archive: [{ name: 'Миг-1', complete: true, progress: 90, chassis: 'транспорт v1', cost: 170, proj_modifiers: [], devices: [], params: [] }],
    archive: [{
      name: "Прототип 1", complete: false, progress: 7, chassis: "тяжелый фрегат v1", cost: 96, questions: 5, progressing: false,
      proj_modifiers: [{ param: "скорость", value: "понижена на 4%", positive: false }, { param: "число орудий", "value": "понижена на 8%", "positive": false }, { "param": "сенсоры", "value": "понижена на 1%", "positive": false }],
      chassis_modifiers: [{ "param": "скорость", "value": "повышена на 63%", "positive": true }],
      devices: [{ count: "", name: "тяжелый фрегат v1" }, { count: "9x", name: "  Ядерный реактор v2" }, { count: "", name: "тяжелый Ядерные ракеты v1" }, { count: "", name: "Электромагнитные сенсоры v1" }, { count: "", name: "тяжелый Углестальная броня v1" }, { count: "", name: "Ударный ядерный двигатель v1" }, { count: "", name: "тяжелый Топливный бак высокого давления v1" }, { count: "", name: "Подвесной грузовой модуль v2" }],
      params: [{ param: "масса", max: 152, value: 96 }, { "param": "объем", "max": 196, "value": 194 }, { "param": "число орудий", "max": 0, "value": 1 }, { "param": "энергия", "max": false, "value": 16 }, { "param": "урон", "max": false, "value": 10 }, { "param": "грузоподьемность", "max": false, "value": 10 }, { "param": "топливо", "max": false, "value": 12 }, { "param": "скорость", "max": false, "value": 7 }, { "param": "сенсоры", "max": false, "value": 1 }, { "param": "броня", "max": false, "value": 2 }]
    }],
    departments: [{ task: 0, project: 0, enthusiasm: 75 }, { task: 1, project: -1, enthusiasm: 75 }, { task: 0, project: -1, enthusiasm: 75 }, { task: 1, project: -1, enthusiasm: 75 }],
    nomenclature: [{ name: "корпуса", list: [{ name: "транспорт v1", line: "транспорт", chassis_modifiers: [{ "param": "скорость", "value": "повышена на 63%", "positive": true }], params: [{ param: "масса", max: 152, value: 96 }, { "param": "объем", "max": 196, "value": 194 }, { "param": "число орудий", "max": 0, "value": 1 }, { "param": "энергия", "max": false, "value": 16 }, { "param": "урон", "max": false, "value": 10 }, { "param": "грузоподьемность", "max": false, "value": 10 }, { "param": "топливо", "max": false, "value": 12 }, { "param": "скорость", "max": false, "value": 7 }, { "param": "сенсоры", "max": false, "value": 1 }, { "param": "броня", "max": false, "value": 2 }] }, { name: "транспорт v2", proj_modifiers: [], params: [] },] }]
  },

  methods: {
    next_day: function () {
      app.waiting = true;
      ws.send('d');
    },
    next_week: function () {
      app.waiting = true;
      ws.send('w');
    },
    reset: function () {
      app.waiting = true;
      ws.send('r');
    },
    show_project: function (proj) {
      app.page = "archive";
      app.sel_project = proj;
    },
    show_order: function (dep) {
      app.modal_department = dep.id;
    },
    cancel_order: function () {
      console.log(app.modal_department);
      app.modal_department = -1
    },
    perform_order: function () {
      app.waiting = true;
      ws.send("o " + (1 + app.modal_department) + " " + (1 + app.order_task) + " " + (app.order_project + 1));
      app.modal_department = -1
    },

    find_param: function (project, param) {
      for (let key of project.params) {
        if (key.param == param)
          return key.value;
      }
      return 0;
    },
    task_color: function (task_value, cur_value) {
      return cur_value >= task_value ? "text-success" : "text-error";
    },
    param_value: function (item) {
      if (item.max)
        return '' + item.value + " (из " + item.max + ")";
      else
        return item.value
    },
    param_color: function (item) {
      if (item.max)
        if (item.value <= item.max)
          return 'black';
        else
          return 'red';
      else
        return 'black';
    },
    find_device: function (text) {
      if (app === undefined) return { name: "", params: {} };
      for (let slot of app.nomenclature) {
        for (let device of slot.list) {
          if (device.name == text)
            return device;
        }
      }
      return { name: "", params: {} };
    }

  },

  computed: {
    production_info: function () {
      vm = this;
      return vm.production.map(function (el, i) {
        enemy_el = vm.enemy_production[i];
        return {
          has_project: el.project >= 0,
          project: el.project < 0 ? { name: "---нет---", chassis: "", cost: 0, params: [] } : vm.archive[el.project],
          project_id: el.project,
          task: vm.tasks[i],
          power: el.efficiency,
          efficiency: el.project >= 0 ? Math.round(100 * el.efficiency / vm.archive[el.project].cost) / 100 : 0,
          enemy_name: enemy_el.name == "" ? "---нет---" : enemy_el.name,
          enemy_power: enemy_el.efficiency,
          enemy_efficiency: el.name != "" ? Math.round(100 * enemy_el.efficiency / enemy_el.cost) / 100 : 0,
          efficiency_bar: { background: "#81d59a", width: el.efficiency_rate + "%" },
          cost_bar: { background: "#81d59a", width: el.cost_rate + "%" }
        };
      });
    },
    departments_info: function () {
      vm = this;
      return vm.departments.map(function (el, i) {
        proj = el.project < 0 ? { name: "Идет поиск...", chassis: "", cost: 0, params: [] } : vm.archive[el.project];
        return {
          id: i,
          name: "Отдел " + (i + 1),
          has_project: el.project >= 0,
          project_id: el.project,
          project: proj,
          task: el.task === null ? { name: '--нет--', details: [] } : vm.tasks[el.task],
          enthusiasm: el.enthusiasm,
          enthusiasm_bar: { background: "#81d59a", width: el.enthusiasm + "%" },
          progress_bar: { background: proj.progressing ? "#81d59a" : "red", width: proj.progress + "%" },
          questions_bar: { background: "khaki", width: proj.questions * 10 + "%" }
        };
      });
    },
  },
});

const ws = new WebSocket('ws://localhost:3000/ws');

ws.onopen = event => {
  // alert('onopen');
  // ws.send("Hello Web Socket!");
};

ws.onerror = event => {
  // alert(event.data);
};

ws.onmessage = event => {
  all = JSON.parse(event.data);
  app.day = all.day;
  app.victory = all.victory;
  app.news = all.news;
  app.tasks = all.tasks;
  app.production = all.production;
  app.enemy_production = all.enemy_production;
  app.archive = all.archive;
  app.departments = all.departments;
  app.nomenclature = all.nomenclature;
  app.waiting = false;
};

