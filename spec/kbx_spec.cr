require "./spec_helper"

include Kbx

size1 = Size.new("small", "")
size2 = Size.new("big", "")
param1 = Param.new("theparamgood", "", {size1 => 0.1, size2 => 1.0})
param2 = Param.new("theparamuse", "", {size1 => 1.0, size2 => 1.0})

describe Param do
  it "depends on size" do
    param1.kscale[size2].should be > param1.kscale[size1]
    param2.kscale[size2].should eq param2.kscale[size1]
  end
end

slot = Slot.new(0, "theslot", "", [size1, size2], Chassis)
slot.add_param(param1, 1.0..10.0, 10.0..100.0)
slot.add_param(param2, 10.0..30.0, 1.0..3.0)

line1 = DeviceLine.new("theline", "", slot, 5)
chassis1 = Chassis.new("thechassis", 1, "", line1, size1, 1.0)

project1 = Project.new("theproject", "", chassis1)
project1.params[param1] = 10

describe ConstraintMinValue do
  it "checks value" do
    ConstraintMinValue.new(param1, 5).satisfied(project1).should be_true
    ConstraintMinValue.new(param1, 15).satisfied(project1).should be_false
  end
  it "depends on param" do
    ConstraintMinValue.new(param1, 5).depends(param1).should be_true
    ConstraintMinValue.new(param1, 5).depends(param2).should be_false
  end
end

describe ConstraintMaxValue do
  it "checks value" do
    ConstraintMaxValue.new(param1, 5).satisfied(project1).should be_false
    ConstraintMaxValue.new(param1, 15).satisfied(project1).should be_true
  end
end

describe Slot do
  it "generate limits" do
    slot.range_for(param1, 10).should eq 10.0..100.0
    slot.range_for(param2, 1).should eq 10.0..30.0

    slot.range_for(param1, 5).should eq 5.0..50.0
    slot.range_for(param2, 5).should eq 2.0..6.0
  end
  it "generate limits for negative values" do
    slot2 = Slot.new(0, "theslot", "", [size1, size2], Chassis)

    slot2.add_param(param1, -1.0..-10.0, -10.0..-100.0)
    slot2.range_for(param1, 5).should eq -50.0..-5.0

    slot2.add_param(param2, -10.0..-30.0, -1.0..-3.0)
    slot2.range_for(param2, 5).should eq -6.0..-2.0
  end
end

describe DeviceLine do
  it "has bugs" do
    line1.bugs.should be_close 0.25, 0.25
  end

  it "generates modifiers" do
    line1.modifiers[param1].should be_close 1.0, TWEAK_K
    line1.modifiers[param2].should be_close 1.0, TWEAK_K
  end
  it "generate params value" do
    ((1 - TWEAK_K)*5.0..(1 + TWEAK_K)*60.0).should contain line1.generate_value(param1)
    ((1 - TWEAK_K)*2.0..(1 + TWEAK_K)*6.0).should contain line1.generate_value(param2)
  end
end

device1 = Device.new("device1", 1, "", line1, size1, 1.0)
device2 = Device.new("device2", 1, "", line1, size2, 1.0)
describe Device do
  it "generate param values" do
    (((1 - TWEAK_K)*5.0).to_i..((1 + TWEAK_K)*60.0).to_i).should contain device2.params[param1]
    (((1 - TWEAK_K)*2.0).to_i..((1 + TWEAK_K)*6.0).to_i).should contain device1.params[param2]
    (((1 - TWEAK_K)*5.0/10).to_i..((1 + TWEAK_K)*60.0/10).to_i).should contain device1.params[param1]
  end
end

describe Chassis do
  it "generate modifiers" do
    chassis1.modifiers[param2].should be_close 1.0, TWEAK_K*2
  end
end

describe Project do
  it "calc params" do
    project1.calc_params
    project1.params[param2].should eq (chassis1.params[param2] * chassis1.modifiers[param2]).to_i

    project1.devices << device1
    project1.devices << device2
    project1.calc_params
    project1.params[param1].should eq ((chassis1.params[param1] + device1.params[param1] + device2.params[param1])*chassis1.modifiers[param1]).to_i
  end
end

describe Task do
  it "reject wrong projects" do
    task1 = Task.new("thetask", "")
    project1.params[param1] = 10
    project1.params[param2] = 20
    task1.limits << ConstraintMinValue.new(param1, 15)
    task1.rate(project1).should be_nil
  end
  it "rate right projects" do
    task1 = Task.new("thetask", "")
    project1.params[param1] = 5
    project1.params[param2] = 2
    task1.optimality[param1] = 0.1
    task1.optimality[param2] = 10
    task1.rate(project1).should eq Math.log(5)*0.1 + Math.log(2)*10
  end
end

describe Project do
  it "eventually satisfies task or fails" do
    project1 = Project.new("theproject", "", chassis1)
    project1.devices << device1
    project1.devices << device2
    project1.calc_params
    task1 = Task.new("thetask", "")
    task1.limits << ConstraintMinValue.new(param1, (project1.params[param1]*1.1).to_i)
    task1.rate(project1).should be_nil
    oldparams = project1.params

    project1.add_progress(task1)
    project1.progress.should eq 0
    days = 0
    100.times { project1.add_progress(task1); days += 1; break if project1.complete? || project1.questions >= TWEAK_MAX_QUESTIONS }

    if project1.progressing
      task1.rate(project1).should be_a Float64
    else
      project1.questions.should be >= TWEAK_MAX_QUESTIONS
    end
  end

  it "progresses when all conditions satisfied" do
    project1 = Project.new("theproject", "", chassis1)
    project1.devices << device1
    project1.devices << device2
    project1.calc_params
    task1 = Task.new("thetask", "")
    task1.limits << ConstraintMinValue.new(param1, (project1.params[param1]*0.8).to_i)
    project1.add_progress(task1)
    project1.progress.should be > 0
    500.times { project1.add_progress(task1); break if project1.complete? }
    task1.rate(project1).should be_a(Float64)
  end

  it "shows name" do
    project1 = Project.new("theproject", "", chassis1)
    project1.devices << device1
    project1.devices << device2
    project1.params[param1] = 10
    project1.params[param2] = 20
    project1.modifiers[param1] = 1.15
    project1.modifiers[param2] = 0.95
    project1.progress = 0.25
    project1.to_s.should eq "theproject: (25%)
ТТХ: theparamgood: 10, theparamuse: 20
(theparamgood повышена на 14%, theparamuse понижена на 5%)
thechassis, device1, device2"
  end
end
