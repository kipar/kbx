require "./spec_helper"

include Kbx

describe Bureau do
  it "apply ChangeRequirements event" do
    b = Bureau.new(DemoSpace.new, "")
    b.log_news = false
    10.times { Events::ChangeRequirements.new.happens(b) }
    p b.news_feed
  end
end
