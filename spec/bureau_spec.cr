require "benchmark"
require "./spec_helper"

include Kbx

describe Bureau do
  it "creates" do
    Bureau.new(DemoSpace.new, "")
  end
  it "updates" do
    b = Bureau.new(DemoSpace.new, "")
    b.update
  end
  it "generates random project" do
    b = Bureau.new(DemoSpace.new, "")
    b.gen_project(b.random_chassis, 1.0).params.values.all?(&.>=(0)).should be_true
  end
  it "generates random project for task (or fails)" do
    b = Bureau.new(DemoSpace.new, "")
    task = b.random_task
    #    puts task
    proj1 = b.gen_project(b.random_chassis, 0.9)
    n = 1
    proj2 = b.gen_good_project(task, 0.5 + rand*0.6, 10)
    loop do
      break if proj2
      proj2 = b.gen_good_project(task, 0.5 + rand*0.6, 10)
      n += 1
      break if n > 10
    end
    task.rate_with_penalty(proj2).should be > task.rate_with_penalty(proj1) if proj2
  end

  it "generates random projects fast" do
    b = Bureau.new(DemoSpace.new, "")
    puts Benchmark.measure { b.gen_good_project(b.random_task, 1.0, 1000) }
  end

  it "random projects can be progressed" do
    b = Bureau.new(DemoSpace.new, "")
    task = b.random_task
    proj = b.gen_good_project(task, 1.2, 10)
    proj = b.gen_good_project(task, 0.9, 10) unless proj
    proj.add_progress task if proj
  end

  pending "departments generate projects" do
    b = Bureau.new(DemoSpace.new, "")
    b.departments[0].project.should eq nil
    b.update
    b.departments.any? { |d| d.project.nil? }.should be_true
    500.times { |i| b.update }
    puts b.production.select { |key, value| value }.map { |key, value| value.not_nil!.desc_for(key) }
    puts b.production.select { |key, value| value }.map { |key, value| value.not_nil!.chassis.name }.tally
    b.production.values.all?(&.nil?).should be_false
  end

  it "can clone projects" do
    b = Bureau.new(DemoSpace.new, "")
    x = b.gen_project(b.random_chassis, 1.0)
    x.add_progress b.production.keys.sample
    y = b.duplicate_project(x)
    y.params.should eq x.params
  end

  it "can clone good projects" do
    b = Bureau.new(DemoSpace.new, "")
    task = b.production.keys.sample
    x = nil
    loop do
      x = b.gen_good_project(task, 0.9, 100)
      break if x
      b = Bureau.new(DemoSpace.new, "")
      task = b.production.keys.sample
    end
    x = x.not_nil!
    x.add_progress task
    y = b.duplicate_project(x)
    y.params.should eq x.params
  end

  it "can rework projects" do
    b = Bureau.new(DemoSpace.new, "")
    x = b.gen_project(b.random_chassis, 1.0)
    x.add_progress b.production.keys.sample
    y = b.rework_project(x, b.production.keys.sample, 10)
    y.params.should_not eq x.params
    y.devices.size.should be > 0
  end
end
