require "./spec_helper"

include Kbx

setting = DemoSpace.new

describe DemoSpace do
  it "compiles" do
    setting.should be_a Setting
  end

  it "has a slot for Chassis" do
    setting.all_slots.any? { |slot| slot.device_class == Chassis }.should be_true
  end

  setting.all_params.all? do |param|
    it "has a slot for #{param}" do
      setting.all_slots.any? { |slot| slot.valueN[param]? && slot.valueN[param].end > 0 }.should be_true
    end
  end
  setting.all_slots.all? do |slot|
    it "has lines for #{slot}" do
      setting.all_lines.count { |line| line.slot == slot }.should be > 1
    end
  end
  nweapons = setting.all_params[3]
  damage = setting.all_params[4]
  it "weapons has 1 nweapons" do
    setting.all_lines.each do |line|
      next unless line.slot.valueN[damage]?
      line.sizes.each do |size|
        dev = line.slot.device_class.new("", 1, "", line, size, 1.0)
        dev.params[nweapons].should eq -1
      end
    end
  end
end
