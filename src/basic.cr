require "json"

MAX_LEVEL      =  10
TWEAK_K        = 0.1
TWEAK_CHANGE   = 0.2
TWEAK_PROGRESS = 0.1
N_DEPARTMENTS  =   4

RATE_DEADPROJ = 1000
RATE_PENALTY  =    5

PROJ_CREATION_CHANCE   = 0.3
PROJ_CREATION_COUNT    = 200
PROJ_CREATION_MODIFIER = 0.9..1.05

PROJ_REWORK_COUNT = 100

FIND_DEVICE_COUNT = 10

TWEAK_MIN_MOD       = 0.2
TWEAK_MAX_MOD       = 2.5
TWEAK_MAX_QUESTIONS =  10
TWEAK_REQUIREMENTS  = 1.0

BUREAU_NAMES = ["Миг-", "F-", "Jao-", "Ü-"]
BUREAU_DESC  = ["Россия", "США", "Китай", "ЕС"]

module Kbx
  class GameObject
    getter name : String
    getter desc : String

    def initialize(@name, @desc)
    end

    def to_s(io)
      io << name
    end

    def inspect(io)
      io << name
    end
  end

  class Size < GameObject
    getter affecting = [] of Param
  end

  @[Flags]
  enum ParamFlags
    ShowUsed
    Required
    IsCost
    MassScaled
    IsMass
  end

  class Param < GameObject
    getter kscale : Hash(Size, Float64)
    getter flags : ParamFlags

    def initialize(@name, @desc, @kscale, @flags = ParamFlags.new(0))
      kscale.keys.each { |sz| sz.affecting << self }
    end

    def for_json(value)
      {param: @name, max: false, value: @flags.show_used? ? value.abs : value}
    end

    def for_json_used(values)
      {param: @name, max: values[0], value: values[1]}
    end

    def for_json_percent(value)
      v = ((value - 1)*100).to_i
      return nil if v == 0
      {param: @name, value: "#{v > 0 ? "повышена" : "понижена"} на #{v.abs}%", positive: v > 0}
    end
  end

  class Setting
    getter all_params = [] of Param
    getter all_sizes = [] of Size
    getter all_lines = [] of DeviceLine
    getter all_slots = [] of Slot
    getter all_tasks = [] of TaskTemplate

    def chassis
      all_slots[0]
    end
  end
end

def scale_scalar(v0, vN, level)
  if v0.abs <= vN.abs
    return (vN - v0) * (level - 1) / (MAX_LEVEL - 1) + v0
  else
    inv = (1.0/vN - 1.0/v0) * (level - 1) / (MAX_LEVEL - 1) + 1.0/v0
    return 1.0/inv
  end
end
