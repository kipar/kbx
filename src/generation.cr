require "./basic"
require "./project"
require "./limit"

module Kbx
  class Bureau
    def random_chassis
      @nomenclature[@setting.chassis].sample.as(Chassis)
    end

    def gen_project(chassis, idealization, requirements = {} of Param => Int32)
      # select basic lines
      Project.new("", "", chassis).tap do |proj|
        used = [] of DeviceLine
        setting.all_params.each do |param|
          proj.modifiers[param] = idealization
        end
        setting.all_slots.each do |slot|
          next if slot == setting.chassis
          possible_lines = lines.select { |line| line.slot == slot && @nomenclature[line.slot].any? { |d| d.line == line } }
          used << possible_lines.sample if possible_lines.size > 0
        end
        fill_project(proj, used, requirements)
      end
    end

    # main function of project generation,
    private def fill_project(proj, used_lines, requirements = {} of Param => Int32)
      devices = used_lines.map { |line| @nomenclature[line.slot].select { |d| d.line == line } }.flatten
      # add devices while it is possible
      # devices.sort_by! { |dev| dev.params.sum { |param, value| value * (rating[param]? || 0) } }
      devices.shuffle
      proj.calc_params
      loop do
        allowed = devices.select { |dev| dev.params.all? { |param, value| value >= -proj.params[param] } }
        break if allowed.size == 0
        bad_params = requirements.keys.select { |param| proj.params[param] < requirements[param] }
        limit_params = requirements.keys.select { |param| proj.params[param] == requirements[param] }
        if bad_params.size > 0
          proj.devices << allowed.weighted_sample do |dev|
            if bad_params.any? { |param| (dev.params[param]? || 0) > 0 }
              1.0
            elsif limit_params.any? { |param| (dev.params[param]? || 0) > 0 }
              0.1
            else
              0.01
            end
          end
        else
          proj.devices << allowed.sample
        end
        proj.calc_params
      end
    end

    def gen_good_project(task, idealization, count)
      chassis = random_chassis
      best = gen_project(chassis, idealization)
      bestv = task.rate_with_penalty best
      required = {} of Param => Int32
      task.optimality.keys.each { |p| required[p] = 1 }
      setting.all_params.select { |param| param.flags.required? }.each { |p| required[p] = 1 }
      task.limits.each { |limit| required[limit.param] = limit.value }
      (count - 1).times do
        chassis = random_chassis
        proj = gen_project(chassis, idealization, required)
        next if required.any? { |param, value| proj.params[param] < 1 }
        rate = task.rate_with_penalty proj
        best, bestv = proj, rate if rate > bestv
      end
      return nil if required.any? { |param, value| best.params[param] <= 0 }
      best.modifiers.clear
      best.calc_params
      best.initial_task = task
      best
    end

    def duplicate_project(project)
      Project.new("", "", project.chassis).tap &.clone_from(project)
    end

    def rework_project(project, task, count)
      required = {} of Param => Int32
      task.optimality.keys.each { |p| required[p] = 1 }
      setting.all_params.select { |param| param.flags.required? }.each { |p| required[p] = 1 }
      task.limits.each { |limit| required[limit.param] = limit.value }
      Project.new("", "", project.chassis).tap do |x|
        x.clone_from(project)
        x.progress = 0.0
        used = project.devices.reject(&.is_a?(Chassis)).map(&.line).uniq!
        # TODO - replace one line
        x.devices.clear
        x.devices << x.chassis
        fill_project(x, used)
        best = x.devices.dup
        bestr = task.rate_with_penalty(x)
        (count - 1).times do
          x.devices.clear
          x.devices << x.chassis
          x.calc_params
          fill_project(x, used, required)
          next if required.any? { |param, value| x.params[param] <= 0 }
          rate = task.rate_with_penalty(x)
          best, bestr = x.devices.dup, rate if rate > bestr
        end
        x.devices.replace best
        x.initial_task = task
        x.calc_params
      end
    end
  end
end
