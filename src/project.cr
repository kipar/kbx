require "./basic"
require "./device"

module Kbx
  class Project < GameObject
    property chassis : Chassis
    getter devices = [] of Device
    property progress = 0.0
    getter progressing = false
    property questions = 0
    property complete_day = 0
    property victory = 0

    property useful = false
    property wellfit = false
    property initial_task : Task?

    getter params = Hash(Param, Int32).new(0)
    getter used_params = Hash(Param, Tuple(Int32, Int32)).new
    getter modifiers = Hash(Param, Float64).new(1.0)

    def rename(name)
      @name = name
    end

    def complete?
      @progress >= 1.0
    end

    def calc_params
      @params.clear
      positive = Hash(Param, Float64).new(0.0)
      negative = Hash(Param, Float64).new(0.0)
      all_params = Set(Param).new
      @devices.each do |device|
        device.params.each do |param, value|
          all_params << param
          if value >= 0
            positive[param] += value
          else
            negative[param] += -value
          end
        end
      end
      all_params.each do |param|
        @params[param] = (@chassis.modifiers[param] * @modifiers[param] * positive[param]).to_i - negative[param].to_i
        @params[param] = @params[param] // mass if param.flags.mass_scaled?
        @used_params[param] = {(@chassis.modifiers[param] * @modifiers[param] * positive[param]).to_i, negative[param].to_i} if param.flags.show_used?
      end
    end

    def cost
      @params.keys.select { |param| param.flags.is_cost? }.map do |param|
        param.flags.show_used? ? @used_params[param][1] : @params[param]
      end.max? || 1
    end

    def mass
      @params.keys.select { |param| param.flags.is_mass? }.map do |param|
        param.flags.show_used? ? @used_params[param][1] : @params[param]
      end.max? || 1
    end

    def initialize(@name, @desc, @chassis)
      @devices << @chassis
    end

    private def roll_change
      v = roll_negative
      rand < 0.5 ? v : {v[0], 1.0/v[1]}
    end

    private def roll_negative
      {params.keys.sample, 1 - rand*TWEAK_CHANGE * (1 - @progress)}
    end

    private def apply_change(aparam, adelta)
      @modifiers[aparam] = (@modifiers[aparam]*adelta).clamp(TWEAK_MIN_MOD*0.9, TWEAK_MAX_MOD*1.1)
      calc_params
    end

    def add_progress(task)
      badparams = task.optimality.keys.select { |par| @params[par] <= 0 } + task.limits.select { |x| !x.satisfied(self) }.map(&.param)
      badparams = (badparams + @params.keys.select { |par| @params[par] < 0 }).uniq!
      goodparams = params.keys - badparams
      @questions = TWEAK_MAX_QUESTIONS if goodparams.size == 0
      aparam, adelta = roll_change
      @progressing = badparams.size == 0
      if badparams.size > 0
        @questions += 1
        # reroll positive if there are unsatisfied
        if adelta > 1 && !params.includes?(aparam)
          if rand < 0.5
            aparam = badparams.sample
          else
            aparam, adelta = roll_negative
          end
        end
      else
        @progress += rand*TWEAK_PROGRESS
        @progress = 1.0 if @progress > 1.0
      end

      if adelta < 1
        if badparams.includes?(aparam) && goodparams.size > 0
          aparam = goodparams.sample
        end
      end
      apply_change(aparam, adelta)
    end

    def show_progress # (task)
      return "Готов" if complete?
      (@progress*100).to_i.to_s + "%"
    end

    def show_ttx
      @params.join(", ") do |param, v|
        if param.flags.show_used?
          "#{param.name}: #{@used_params[param][1]} (из #{@used_params[param][0]})"
        else
          "#{param.name}: #{v}"
        end
      end
    end

    def show_devices
      @devices.tally.join(", ") { |d, count| (count > 1 ? "#{count}x " : "") + d.name }
    end

    def show_modifiers
      @modifiers.join(", ") do |param, value|
        v = ((value - 1)*100).to_i
        next if v == 0
        "#{param.name} #{(v > 0 ? "повышена" : "понижена")} на #{v.abs}%"
      end
    end

    def roll_bugs
      rand < @devices.sample.bugs
    end

    def desc_for(task)
      "#{@name}: #{@chassis} (пригодность как #{task.name} #{task.exprate(self)}, стоимость #{cost})"
    end

    def for_archive(bureau)
      "#{@name}: #{@chassis} (#{params})"
    end

    def to_s(io)
      io << @name << ": (" << show_progress << ")" << '\n'
      io << "ТТХ: " << show_ttx << '\n'
      io << "(" << show_modifiers << ")" << '\n'
      io << show_devices
    end

    def clone_from(x)
      @chassis = x.chassis
      @devices = x.devices.dup
      @progress = x.progress
      @progressing = false
      @questions = x.questions
      @complete_day = 0
      @modifiers.clear
      @modifiers.merge! x.modifiers
      # @modifiers = x.modifiers.clone

      calc_params
    end

    def for_json
      {name:              @name,
       complete:          complete?,
       day:               @complete_day,
       questions:         @questions,
       progressing:       @progressing,
       progress:          (@progress*100).to_i,
       chassis:           @chassis.name,
       cost:              cost,
       victory:           victory,
       proj_modifiers:    @modifiers.map { |par, value| par.for_json_percent(value) }.reject(&.nil?),
       chassis_modifiers: @chassis.modifiers.map { |par, value| par.for_json_percent(value) }.reject(&.nil?),
       devices:           @devices.tally.map { |d, count| {({count: (count > 1 ? "#{count}x " : ""), name: d.name}), d} }.sort_by { |d| d[1].line.slot.index }.map(&.first),
       params:            @params.map { |par, value| par.flags.show_used? ? par.for_json_used(@used_params[par]) : par.for_json(value) }}
    end
  end
end
