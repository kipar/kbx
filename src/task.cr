require "./basic"
require "./project"
require "./limit"

module Kbx
  class TaskTemplate < GameObject
    getter important = Hash(Param, Int32).new(0)
    getter limited = Set(Param).new

    def initialize(@name, @desc, important, limited)
      important.each_with_index do |x, i|
        @important[x] = i
      end
      limited.each do |x|
        @limited << x
      end
    end

    def generate : Task
      Task.new(@name, @desc).tap do |x|
        @important.each do |param, v|
          x.optimality[param] = 1.0 / (v + 1)
        end
        @limited.each do |param|
          if rand < 0.5
            x.optimality[param] = 0.1
          else
            x.limits << ConstraintMinValue.new(param, 1)
          end
        end
      end
    end
  end

  class Task < GameObject
    getter limits = [] of Constraint
    getter optimality = {} of Param => Float64

    def rate(project)
      return nil if project.params.values.any? &.< 0
      return nil unless limits.all? &.satisfied(project)
      return optimality.sum { |param, k| Math.log(project.params[param] || 0.001)*k } - Math.log(project.cost)
    end

    def rate_with_penalty(project)
      return -RATE_DEADPROJ if project.params.values.any? &.< 0
      # return -RATE_DEADPROJ if project.params.any? { |param, value| param.flags.IsRequired && value == 0 }
      v = optimality.sum { |param, k| Math.log({project.params[param] || 0.001, 0.001}.max)*k } - RATE_PENALTY*limits.sum { |lim| Math.log(lim.penalty(project) + 1) } - Math.log(project.cost)
    end

    def exprate(project)
      Math.exp(rate_with_penalty(project)) * project.cost
    end

    def percentrate(project, with_cost)
      rate = rate_with_penalty(project)
      rate += Math.log(project.cost) unless with_cost
      v = if rate < 0
            Math.exp(rate)/2
          else
            1.0 - Math.exp(-rate)
          end
    end

    def to_s(io)
      io << name
      io << " ( " << optimality
      io << " " << limits.join(", ", &.to_s)
      io << " )"
    end

    def difficulty
      optimality.keys.size + limits.size + limits.count { |x| x.value > 1 }
    end

    def for_json
      {name: @name, details: optimality.map { |par, value| {param: par.name, value: 1, text: "Приоритет ##{(1.0/value).to_i}: #{par.name}"} } + limits.map { |limit| {param: limit.param.name, value: limit.value, text: limit.to_s} }}
    end
  end
end
