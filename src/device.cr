require "./basic"

module Kbx
  class Slot < GameObject
    getter value0 = {} of Param => Range(Float64, Float64)
    getter valueN = {} of Param => Range(Float64, Float64)
    getter sizes : Array(Size)
    getter device_class : Device.class
    property index : Int32

    def add_param(param, range0, rangeN)
      value0[param] = Float64.new(range0.begin)..Float64.new(range0.end)
      valueN[param] = Float64.new(rangeN.begin)..Float64.new(rangeN.end)
    end

    def initialize(@index, @name, @desc, @sizes, @device_class = Device)
    end

    def range_for(param, level)
      r0 = value0[param]
      rN = valueN[param]
      v1 = scale_scalar(r0.begin, rN.begin, level)
      v2 = scale_scalar(r0.end, rN.end, level)
      v1, v2 = v2, v1 if v1 > v2
      v1..v2
    end
  end

  class DeviceLine < GameObject
    getter slot : Slot
    getter level : Int32
    property bugs : Float64
    getter sizes : Array(Size)
    getter modifiers = Hash(Param, Float64).new(1.0)

    def initialize(@name, @desc, @slot, @level, tweak : Hash? = nil, asizes : Array(Size)? = nil)
      @sizes = asizes ? asizes : @slot.sizes
      @bugs = rand() / 2
      @slot.value0.keys.each do |param|
        @modifiers[param] = 1.0 - TWEAK_K + rand*TWEAK_K*2
      end
      if tweak
        tweak.each do |param, value|
          @modifiers[param] *= value
        end
      end
    end

    def generate_value(param)
      rand(@slot.range_for(param, @level)) * @modifiers[param]
    end

    def stability(where)
      ((1 - @bugs * where.bugfixes[self])*100).to_i
    end
  end

  class Device < GameObject
    getter line : DeviceLine
    getter size : Size
    property bugs : Float64
    getter params : Hash(Param, Int32)
    getter revision : Int32

    def initialize(@name, @revision, @desc, @line, @size, bugfixes)
      @params = {} of Param => Int32
      @bugs = @line.bugs * bugfixes
      @line.slot.value0.keys.each do |param|
        @params[param] = (@line.generate_value(param) * param.kscale[@size]).to_i
      end
    end

    def stability
      ((1 - @bugs)*100).to_i
    end

    def inspect(io)
      io << @name << ": ("
      io << @params.join(", ") { |param, v| param.name + " " + v.to_s }
      io << ")"
    end

    def for_json
      {name:      @name,
       line:      @line.name,
       stability: stability,
       params:    params.map { |par, value| par.for_json(value) }}
    end
  end

  class Chassis < Device
    getter limits = [] of Constraint
    getter modifiers = Hash(Param, Float64).new(1.0)

    def initialize(name, revision, desc, line, size, bugfixes)
      super(name, revision, desc, line, size, bugfixes)
      line.modifiers.each do |param, value|
        next if @params.keys.includes? param
        @modifiers[param] = (1.0 - TWEAK_K + rand*TWEAK_K*2)*value
      end
      @size.affecting.each do |param|
        next if @params.keys.includes? param
        @modifiers[param] *= param.kscale[@size]
      end
    end

    def for_json
      {name:      @name,
       line:      @line.name,
       stability: stability,
       modifiers: modifiers.map { |par, value| par.for_json_percent(value) }.reject(&.nil?),
       params:    params.map { |par, value| par.for_json(value) }}
    end
  end
end
