require "./basic"
require "./project"
require "./limit"
require "./task"

module Kbx
  class Department
    getter name : String
    getter parent : Bureau
    property task : Task?
    property project : Project?
    property enthusiasm = 100

    def initialize(@parent, id)
      @name = "Отдел #{id}"
    end

    def update
      @enthusiasm += 1 if @enthusiasm < 100
      return if rand(100) >= @enthusiasm

      task = @task || @parent.random_task
      @task = task

      if proj = @project
        # work on project
        if proj.roll_bugs
          @parent.bugs_effect += 1
          # @parent.news(name, "Прогресс по #{proj.name}: задержка в разработке из-за нестабильности компонентов")
          return
        end
        proj.add_progress task
        if proj.complete?
          @parent.news(name, "Завершен проект: #{proj.desc_for(task)}, запущен в производство под именем #{@parent.name_for(proj)}")
          proj.complete_day = @parent.day
          @project = nil
          @task = nil
        else
          if proj.questions >= TWEAK_MAX_QUESTIONS
            @parent.failed_projects += 1
            @parent.news(name, "Прогресс по #{proj.name}: разработка зашла в тупик и прекращена")
            @parent.archive.delete proj
            @project = nil
            @task = nil
          else
            if proj.progressing
              # @parent.news(name, "Прогресс по #{proj.name}: степень завершения #{(proj.progress*100).to_i}")
            else
              # @parent.news(name, "Прогресс по #{proj.name}: поиск технического решения, сомнения #{proj.questions}") # #{proj.modifiers} #{task.limits}")
            end
          end
        end
      else
        # generate new project
        return if rand > PROJ_CREATION_CHANCE
        @project = @parent.gen_good_project(task, rand(PROJ_CREATION_MODIFIER), PROJ_CREATION_COUNT)
        if proj = @project
          @parent.name_for(proj)
          @parent.archive << proj
          @parent.news(name, "Разработан эскиз: #{proj.desc_for(task)}")
        end
      end
    end

    def choose_work
      @project = nil
      @task = @parent.random_task
    end

    def inspect(io)
      io << "#{name}, энтузиазм #{@enthusiasm}, задача #{task}, проект: #{project.to_s}"
    end

    def short_desc
      "#{name}, энтузиазм #{@enthusiasm}, задача: #{task.inspect}, проект: #{project.inspect}"
    end

    def for_json
      {task:       @parent.production.keys.index(task),
       project:    project ? @parent.archive.index(@project) : -1,
       enthusiasm: enthusiasm}
    end
  end

  class Bureau
    getter name : String
    getter desc : String
    getter setting : Setting
    property techlevel = 1
    getter archive = [] of Project
    getter production = {} of Task => Project?
    getter nomenclature = {} of Slot => Array(Device)
    getter lines = [] of DeviceLine
    getter departments = [] of Department
    getter project_revisions = Hash(String, Int32).new(0)
    property log_news = true
    getter news_feed = [] of {origin: String, text: String, important: Bool, color: String}
    getter day = 1
    getter bugfixes = Hash(DeviceLine, Float32).new(1.0)
    property victory = 50
    property failed_projects = 0
    property bugs_effect = 0

    def initialize(@setting, @name, @desc = "", tasks = nil)
      if tasks
        tasks.each do |task|
          @production[task] = nil
        end
      else
        @setting.all_tasks.each do |template|
          @production[template.generate] = nil
        end
      end
      @setting.all_slots.each do |slot|
        @nomenclature[slot] = ([] of Device)
      end
      @lines = @setting.all_lines.select { |x| x.level <= @techlevel }
      @lines.each do |line|
        line.sizes.each do |size|
          dev = line.slot.device_class.new("#{size.name} #{line.name} v1", 1, "", line, size, @bugfixes[line])
          @nomenclature[line.slot] << dev
        end
      end

      N_DEPARTMENTS.times { |i| departments << Department.new(self, i + 1) }
      departments.each(&.choose_work)

      fill_events
    end

    def random_task
      mind = production.keys.reject { |task| @departments.any? { |dep| dep.task == task } }.min_of(&.difficulty)
      production.keys.weighted_sample do |task|
        x = if production.values.all?(&.nil?)
              task.difficulty == mind ? 1.0 : 0.0
            else
              1.0 / task.difficulty
            end
        x /= 2 if production[task]
        x /= 2 if @departments.any? { |dep| dep.task == task }
        x
      end
    end

    def update(andreset = true)
      @day += 1
      @news_feed.clear if andreset
      departments.each(&.update)
      happen_events
      update_production
    end

    def update_production
      return if @archive.size == 0
      @production.keys.each do |task|
        cur = @production[task]
        best = @archive.max_by? do |proj|
          proj.progress < 1.0 ? 0.0 : task.exprate(proj)
        end
        next unless best
        next if best.progress < 1.0
        next if best == cur
        if task.exprate(best) > (cur ? task.exprate(cur) : 1.0)
          @production[task] = best
          best.useful = true
          best.wellfit = true if best.initial_task == task
          news("Отдел производства", "Проект #{best.name} взят на вооружение в качестве #{task.name}", important: true)
        end
      end
    end

    def is_enemy
      @name != BUREAU_NAMES[0]
    end

    def news(origin, s, *, important = false, color = is_enemy ? "error" : "primary")
      # puts origin + ": " + s if @log_news
      @news_feed << {origin: origin, text: s, important: important, color: color}
    end

    def name_for(proj)
      prefix = proj.progress < 1.0 ? "Прототип " : @name
      project_revisions[prefix] += 1
      (prefix + project_revisions[prefix].to_s).tap { |s| proj.rename(s) }
    end

    def show
      '\n' + @production.join('\n') { |task, value| task.name + ": " + (value ? value.desc_for(task) : "---нет---") } + '\n' +
        @departments.join('\n') { |dep| dep.short_desc } + '\n'
    end
  end
end
