require "./basic"

module Kbx
  abstract class Constraint
    abstract def name : String
    abstract def desc : String
    abstract def satisfied(project) : Bool
    abstract def penalty(project) : Int32

    def depends(param)
      false
    end

    def to_s(io)
      io << name
    end

    def inspect(io)
      io << name
    end
  end

  class ConstraintMinValue < Constraint
    getter param : Param
    property value : Int32

    def initialize(@param, @value)
    end

    def name : String
      "#{param.name} >= #{value}"
    end

    def desc : String
      "Значение #{param.name} должно быть не меньше #{value}"
    end

    def satisfied(project) : Bool
      project.params[param] >= value
    end

    def penalty(project) : Int32
      {value - project.params[param], 0}.max
    end

    def depends(param)
      param == @param
    end
  end

  class ConstraintMaxValue < Constraint
    getter param : Param
    property value : Int32

    def initialize(@param, @value)
    end

    def name : String
      "#{param.name} <= #{value}"
    end

    def desc : String
      "Значение #{param.name} должно быть не больше #{value}"
    end

    def satisfied(project) : Bool
      project.params[param] <= value
    end

    def penalty(project) : Int32
      {project.params[param] - value, 0}.max
    end

    def depends(param)
      param == @param
    end
  end
end
