require "./basic"
require "./project"
require "./limit"
require "./world"

module Enumerable(T)
  def weighted_sample
    weights = map { |item| yield(item) }
    weighted_sample(weights)
  end

  # TODO - iterator support?
  def weighted_sample(weights : (Array | Tuple)) : T
    total = weights.sum
    point = rand * total
    zip(weights) do |n, w|
      return n if w >= point
      point -= w
    end
    return first # to prevent "nillable" complain
  end
end

module Kbx
  TWEAK_EVENTS = {
    Events::OpenLine           => 0.2,
    Events::IncLevel           => 0.2,
    Events::NewDevice          => 1.0,
    Events::DebugLine          => 1.0,
    Events::DebugDevice        => 1.0,
    Events::ChangeRequirements => 1.0,

    Events::TestPower         => 0.5,
    Events::TestPowerAbs      => 0.5,
    Events::TestEfficiency    => 0.5,
    Events::TestEfficiencyRel => 0.5,
    Events::TestPowerStat     => 0.2,
    Events::TestPowerStatAbs  => 0.2,
  }

  abstract class RandomEvent
    abstract def happens(where : Bureau)

    def chance(where : Bureau) : Float64
      1.0
    end
  end

  class Bureau
    getter all_events = Hash(RandomEvent.class, Array(RandomEvent)).new

    def fill_events
      TWEAK_EVENTS.keys.each { |key| @all_events[key] = [Events::DummyEvent.new.as(RandomEvent)] }
      @all_events[Events::OpenLine] += @setting.all_lines.map { |line| Events::OpenLine.new(line) }
      @all_events[Events::IncLevel] += [Events::IncLevel.new]
      @all_events[Events::NewDevice] += @setting.all_lines.map { |line| line.sizes.map { |size| Events::NewDevice.new(line, size) } }.flatten
      @all_events[Events::DebugLine] += @setting.all_lines.map { |line| Events::DebugLine.new(line) }
      @all_events[Events::DebugDevice] += [Events::DebugDevice.new]
      @all_events[Events::ChangeRequirements] += [Events::ChangeRequirements.new]
    end

    def fill_world_events(enemies)
      @all_events[Events::TestPower] += [Events::TestPower.new(enemies)]
      @all_events[Events::TestPowerAbs] += [Events::TestPowerAbs.new(enemies)]
      @all_events[Events::TestEfficiency] += [Events::TestEfficiency.new(enemies)]
      @all_events[Events::TestEfficiencyRel] += [Events::TestEfficiencyRel.new(enemies)]
      @all_events[Events::TestPowerStat] += [Events::TestPowerStat.new(enemies)]
      @all_events[Events::TestPowerStatAbs] += [Events::TestPowerStatAbs.new(enemies)]
    end

    def happen_events
      cls = TWEAK_EVENTS.keys.weighted_sample { |k| TWEAK_EVENTS[k] }
      evt = @all_events[cls].weighted_sample &.chance(self)
      evt.happens self
    end
  end

  module Events
    class DummyEvent < RandomEvent
      def happens(where : Bureau); end
    end

    class OpenLine < RandomEvent
      getter line : DeviceLine

      def initialize(@line)
      end

      def happens(where : Bureau)
        where.lines << @line
        where.news "Исследовательский отдел", "Открыта новая технология: #{line}"
        NewDevice.new(line, @line.sizes.sample).happens(where)
      end

      def chance(where : Bureau) : Float64
        return 0.0 if where.lines.includes?(@line)
        Math.exp(where.techlevel - line.level)
      end
    end

    class IncLevel < RandomEvent
      def happens(where : Bureau)
        where.techlevel += 1
      end
    end

    class NewDevice < RandomEvent
      getter line : DeviceLine
      getter size : Size

      def initialize(@line, @size)
      end

      def happens(where : Bureau)
        current = where.nomenclature[line.slot].find { |x| x.line == @line && x.size == @size }
        rev = current ? current.revision + 1 : 1
        best = current
        FIND_DEVICE_COUNT.times do
          dev = line.slot.device_class.new("#{@size.name} #{@line.name} v#{rev}", rev, "", @line, @size, where.bugfixes[@line])
          if current
            next if dev.params.any? { |param, value| value < current.params[param] }
            next if dev.params.all? { |param, value| value == current.params[param] }
          end
          next if where.nomenclature[line.slot].any? { |other| other.params.all? { |param, value| dev.params[param] <= value } }
          best = dev
        end
        return if best == current
        return unless best
        where.nomenclature[@line.slot].delete current
        where.nomenclature[@line.slot].reject! { |dev| dev.params.all? { |param, value| value <= best.params[param] } } unless best.is_a? Chassis
        where.nomenclature[@line.slot] << best

        where.news "Исследовательский отдел", "Создано новое устройство: #{best.name}"
      end

      def chance(where : Bureau) : Float64
        return 0.0 unless where.lines.includes?(@line)
        if where.lines.find { |li2| li2.slot == @line.slot && li2.level > @line.level }
          return 0.1
        else
          return 1.0
        end
      end
    end

    class DebugLine < RandomEvent
      getter line : DeviceLine

      def initialize(@line)
      end

      def happens(where : Bureau)
        scale = 0.5 + rand/2

        where.bugfixes[@line] *= scale
        where.nomenclature.values.each &.each { |x| x.bugs *= scale if x.line == @line }
        where.news "Исследовательский отдел", "Отлажена технология #{line}, новый уровень стабильности: #{@line.stability(where)}%"
      end

      def chance(where : Bureau) : Float64
        return 0.0 unless where.lines.includes?(@line)
        return 0.0 if @line.stability(where) > 98
        1.0
      end
    end

    class DebugDevice < RandomEvent
      def initialize
      end

      def happens(where : Bureau)
        scale = 0.5 + rand/2
        dev = where.nomenclature.values.flatten.sample
        dev.bugs *= scale
        where.news "Исследовательский отдел", "Отлажено устройство #{dev}, новый уровень стабильности: #{dev.stability}%"
      end
    end

    class ChangeRequirements < RandomEvent
      def initialize
      end

      def happens(where : Bureau)
        task = where.production.keys.select { |x| x.limits.size > 0 }.sample
        params = task.limits.map { |x| x.param }
        oldlimits = task.limits.map { |x| x.value }
        params.each { |param| task.optimality[param] = rand/4 }
        task.limits.each { |lim| lim.value = 1 }
        proj = nil
        10.times do
          proj = where.gen_good_project(task, 0.9, 100)
          break if proj
        end
        return unless proj
        params.each { |param| task.optimality.delete param }
        newlimits = task.limits.map { |x| (proj.params[x.param]*TWEAK_REQUIREMENTS).to_i }
        return if newlimits.any? { |n| n < 1 }
        return if newlimits.zip(oldlimits).all? { |n, ol| n <= ol }
        i = rand(newlimits.size)
        return if newlimits[i] < oldlimits[i]
        task.limits.each_with_index { |lim, i| lim.value = newlimits[i] }
        where.news "Мир", "В связи с обстановкой в мире изменились требования к #{task.name}: #{task.limits}", color: "warning"
      end
    end

    abstract class TestProject < RandomEvent
      @enemies = [] of Bureau

      def initialize(@enemies)
      end

      abstract def get_value(proj : Project?, bureau : Bureau, task : Task) : Float64
      abstract def msg(task : Task, proj : Project?, bureau : Bureau) : String

      def applicable(task : Task)
        true
      end

      def absolute : Bool
        true
      end

      def happens(where : Bureau)
        task = where.production.keys.select { |x| applicable(x) }.sample
        our_value = get_value(where.production[task], where, task)
        e = absolute ? @enemies.max_by { |b| get_value(b.production[task], b, task) } : @enemies.sample
        enemy_value = get_value(e.production[task], e, task)
        return if our_value == enemy_value
        winner = our_value > enemy_value ? where : e
        where.news "Мир", msg(task, winner.production[task], winner), color: "warning"
        if winner == where
          where.victory += 1
          if proj = where.production[task]
            proj.victory += 1
          end
        else
          where.victory -= 1
        end
      end
    end

    class TestPower < TestProject
      def absolute
        false
      end

      def get_value(proj : Project?, bureau : Bureau, task : Task) : Float64
        return 0.0 unless proj
        task.exprate(proj)
      end

      def msg(task : Task, proj : Project?, bureau : Bureau) : String
        "В одиночном противостоянии #{task.name} #{proj.not_nil!.name} превзошел аналогичный корабль противника"
      end
    end

    class TestPowerStat < TestProject
      @param : Param?

      def absolute
        false
      end

      def get_value(proj : Project?, bureau : Bureau, task : Task) : Float64
        return 0.0 unless proj
        if @param.nil?
          @param = task.optimality.keys.sample
        end
        proj.params[@param.not_nil!]
        task.exprate(proj)
      end

      def msg(task : Task, proj : Project?, bureau : Bureau) : String
        "В одиночном противостоянии превосходная #{@param.not_nil!.name} обеспечила #{task.name} #{proj.not_nil!.name} победу".tap do |x|
          @param = nil
        end
      end
    end

    class TestPowerStatAbs < TestProject
      @param : Param?

      def absolute
        true
      end

      def get_value(proj : Project?, bureau : Bureau, task : Task) : Float64
        if @param.nil?
          @param = task.optimality.keys.sample
        end
        list = bureau.production.values.reject!(&.nil?).map(&.not_nil!)
        return 0.0 if list.size == 0
        1.0*list.max_of { |x| x.params[@param.not_nil!] }
      end

      def msg(task : Task, proj : Project?, bureau : Bureau) : String
        proj.victory -= 1 if proj
        list = bureau.production.values.reject!(&.nil?).map(&.not_nil!)
        return "" if list.size == 0
        proj = list.max_by { |x| x.params[@param.not_nil!] }
        proj.victory += 1
        "На прошедшей выставке #{proj.not_nil!.name} продемонстрировал лучшие показатели #{@param.not_nil!.name} среди всех современных моделей".tap do |x|
          @param = nil
        end
      end
    end

    class TestPowerAbs < TestProject
      def absolute
        true
      end

      def get_value(proj : Project?, bureau : Bureau, task : Task) : Float64
        return 0.0 unless proj
        task.exprate(proj)
      end

      def msg(task : Task, proj : Project?, bureau : Bureau) : String
        "На прошедшей выставке #{proj.not_nil!.name} продемонстрировал лучшие показатели среди #{task.name}"
      end
    end

    class TestEfficiency < TestProject
      def absolute
        true
      end

      def get_value(proj : Project?, bureau : Bureau, task : Task) : Float64
        return 0.0 unless proj
        Math.exp(task.rate_with_penalty(proj))
      end

      def msg(task : Task, proj : Project?, bureau : Bureau) : String
        "Импортеры назвали #{proj.not_nil!.name} самым выгодным среди #{task.name}"
      end
    end

    class TestEfficiencyRel < TestProject
      def absolute
        false
      end

      def get_value(proj : Project?, bureau : Bureau, task : Task) : Float64
        return 0.0 unless proj
        # TODO - build count
        Math.exp(task.rate_with_penalty(proj))
      end

      def msg(task : Task, proj : Project?, bureau : Bureau) : String
        "Превосходство дешевых и эффективных #{task.name} #{proj.not_nil!.name} стало причиной значительных успехов #{bureau.desc}"
      end
    end
  end
end
