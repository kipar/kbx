require "./basic"
require "./project"
require "./limit"

module Kbx
  class DemoSpace < Setting
    @heavy = Size.new("тяжелый", "")
    @heavyw = Size.new("тяжелый", "")
    @normal = Size.new("", "")

    private def sc(k, kw = k)
      {@normal => 1.0, @heavy => k, @heavyw => kw}
    end

    def initialize
      # getter all_params = [] of Param

      mass = Param.new("масса", "", sc(1.4), ParamFlags::ShowUsed | ParamFlags::IsCost | ParamFlags::IsMass)
      volume = Param.new("объем", "", sc(1.25), ParamFlags::ShowUsed)
      energy = Param.new("энергия", "", sc(1.0))
      nweapons = Param.new("число орудий", "", sc(1.0), ParamFlags::ShowUsed)
      damage = Param.new("урон", "", sc(1.0, 1.2))
      sensors = Param.new("сенсоры", "", sc(1.0))
      armour = Param.new("броня", "", sc(1.0, 1.5))
      speed = Param.new("скорость", "", sc(0.5, 1.0), ParamFlags::Required | ParamFlags::MassScaled)
      fuel = Param.new("топливо", "", sc(1.0, 1.3), ParamFlags::Required | ParamFlags::MassScaled)
      cargo = Param.new("грузоподьемность", "", sc(1.3))

      @all_params = [
        mass, energy, volume, nweapons, damage, sensors, armour, speed, fuel, cargo,
      ]

      @all_sizes = [@normal, @heavyw]

      chassis = Slot.new(0, "корпус", "", [@normal, @heavy], Chassis)
      chassis.add_param(mass, 100..120, 120..200)
      chassis.add_param(volume, 120..150, 200..300)
      chassis.add_param(nweapons, 1..2, 1..4)

      reactor = Slot.new(1, "реактор", "", [@normal, @heavyw])
      reactor.add_param(mass, -5..-10, -20..-40)
      reactor.add_param(volume, -10..-15, -10..-30)
      reactor.add_param(energy, 5..10, 50..100)

      weapon = Slot.new(2, "орудие", "", [@normal, @heavyw])
      weapon.add_param(mass, -5..-10, -20..-40)
      weapon.add_param(volume, -10..-15, -20..-30)
      weapon.add_param(energy, -5..-10, -50..-100)
      weapon.add_param(nweapons, -1.3..-1.3, -1.3..-1.3)
      weapon.add_param(damage, 5..10, 50..200)

      sensor = Slot.new(3, "сенсоры", "", [@normal])
      sensor.add_param(mass, -1..-1, -1..-1)
      sensor.add_param(volume, -1..-5, -1..-10)
      sensor.add_param(energy, -5..-10, -50..-100)
      sensor.add_param(sensors, 1..3, 10..50)

      armourplate = Slot.new(4, "броня", "", [@normal, @heavyw])
      armourplate.add_param(mass, -10..-20, -10..-100)
      armourplate.add_param(volume, -10..-20, -40..-200)
      armourplate.add_param(armour, 1..2, 10..50)
      armourplate.add_param(sensors, -1..-1, -5..-10)

      engine = Slot.new(5, "двигатели", "", [@normal])
      engine.add_param(mass, -10..-30, -40..-60)
      engine.add_param(volume, -10..-30, -80..-120)
      engine.add_param(energy, -20..-40, -100..-150)
      engine.add_param(speed, 500..1000, 15000..17500)

      fuelcell = Slot.new(6, "баки", "", [@normal, @heavyw])
      fuelcell.add_param(mass, -10..-10, -10..-10)
      fuelcell.add_param(volume, -20..-20, -40..-80)
      fuelcell.add_param(energy, -5..-10, -10..-80)
      fuelcell.add_param(fuel, 1000..1000, 4000..8000)

      cargopod = Slot.new(7, "контейнеры", "", [@normal])
      cargopod.add_param(mass, -10..-10, -10..-10)
      cargopod.add_param(volume, -20..-40, -50..-80)
      cargopod.add_param(energy, -5..-10, -10..-40)
      cargopod.add_param(cargo, 10..10, 40..60)

      @all_slots = [chassis, reactor, weapon, sensor, armourplate, engine, fuelcell, cargopod]

      frigate = DeviceLine.new("фрегат", "", chassis, 1, {speed => 1.4})
      cruiser = DeviceLine.new("крейсер", "", chassis, 5, {mass => 2, volume => 2, speed => 0.3, fuel => 0.7})
      battleship = DeviceLine.new("линкор", "", chassis, 9, {mass => 5, volume => 5, speed => 0.15})
      transport = DeviceLine.new("транспорт", "", chassis, 1, {mass => 1.25, volume => 1.25, damage => 0.2, fuel => 1.25, cargo => 1.25})
      supertransport = DeviceLine.new("супертранспорт", "", chassis, 6, {mass => 5, volume => 5, damage => 0.2, speed => 0.2})
      yaht = DeviceLine.new("яхта", "", chassis, 4, {mass => 0.5, volume => 0.75, speed => 2.0}, [@normal])
      orgtransport = DeviceLine.new("органический транспорт", "", chassis, 5, {mass => 1.25, volume => 1.25, damage => 0.2, fuel => 1.25, cargo => 1.25})
      orgfrigate = DeviceLine.new("органический фрегат", "", chassis, 5, {speed => 1.6})
      afflictor = DeviceLine.new("эсминец", "", chassis, 3, {nweapons => 3.0, volume => 2, speed => 0.3})

      nuclear = DeviceLine.new("Ядерный реактор", "", reactor, 1)
      thermonuclear = DeviceLine.new("Термоядерный реактор", "", reactor, 2)
      coldnuclear = DeviceLine.new("Реактор холодного синтеза", "", reactor, 3)
      antimatter = DeviceLine.new("Конвертор антиматерии", "", reactor, 4)
      gluon = DeviceLine.new("Глюонный реактор", "", reactor, 5)
      quark = DeviceLine.new("Кварковый реактор", "", reactor, 7)
      hyperstring = DeviceLine.new("Гиперструнный реактор", "", reactor, 10)

      missile = DeviceLine.new("Ядерные ракеты", "", weapon, 1)
      laser = DeviceLine.new("Лазер", "", weapon, 2)
      railgun = DeviceLine.new("Рельсотрон", "", weapon, 3)
      torpedo = DeviceLine.new("Торпеды из антиматерии", "", weapon, 4)
      positron = DeviceLine.new("Позитронный излучатель", "", weapon, 5)
      destructor = DeviceLine.new("Деструктор", "", weapon, 7)
      gravitygun = DeviceLine.new("Гравитационное орудие", "", weapon, 10)

      emsensor = DeviceLine.new("Электромагнитные сенсоры", "", sensor, 1)
      gravsensor = DeviceLine.new("Гравитационные сенсоры", "", sensor, 3)
      qsensor = DeviceLine.new("Квантовые сенсоры", "", sensor, 6)
      hsensor = DeviceLine.new("Сенсоры гиперполя", "", sensor, 8)

      armour1 = DeviceLine.new("Углестальная броня", "", armourplate, 1)
      armour2 = DeviceLine.new("Многослойный панцирь", "", armourplate, 1, {volume => 2, armour => 2})
      armour3 = DeviceLine.new("Наноструктурная броня", "", armourplate, 3)
      armour4 = DeviceLine.new("Монокристаллическая броня", "", armourplate, 4)
      armour5 = DeviceLine.new("Органическая броня", "", armourplate, 5)
      armour6 = DeviceLine.new("Нейтронная броня", "", armourplate, 9, {mass => 2.0, armour => 2.0})
      armour7 = DeviceLine.new("Силовая броня", "", armourplate, 9, {mass => 0.1, energy => 2.0})

      engine1 = DeviceLine.new("Ударный ядерный двигатель", "", engine, 1)
      engine2 = DeviceLine.new("Прямоточный ядерный двигатель", "", engine, 2)
      engine3 = DeviceLine.new("Ионный двигатель", "", engine, 3)
      engine4 = DeviceLine.new("Прыжковый двигатель", "", engine, 4)
      engine5 = DeviceLine.new("Гравитационный двигатель", "", engine, 5)
      engine6 = DeviceLine.new("Квантовый двигатель", "", engine, 6)
      engine7 = DeviceLine.new("Гипердвигатель", "", engine, 10)

      cryocell = DeviceLine.new("Криогенный топливный бак", "", fuelcell, 1)
      pressurecell = DeviceLine.new("Топливный бак высокого давления", "", fuelcell, 1)
      nanocell = DeviceLine.new("Наноструктурный топливный бак", "", fuelcell, 3)
      bioniccell = DeviceLine.new("Бионический топливный бак", "", fuelcell, 5)
      gravitycell = DeviceLine.new("Гравитационный топливный уловитель", "", fuelcell, 6)
      voretiocell = DeviceLine.new("Вореционный топливный бак", "", fuelcell, 10)

      inlinepod = DeviceLine.new("Встроенный грузовой модуль", "", cargopod, 1)
      externalpod = DeviceLine.new("Подвесной грузовой модуль", "", cargopod, 1)
      gravitypod = DeviceLine.new("Гравитационный модуль хранения", "", cargopod, 8)

      @all_lines = [frigate, cruiser, battleship, transport, supertransport, yaht, orgfrigate, orgtransport, afflictor,
                    nuclear, thermonuclear, coldnuclear, antimatter, gluon, quark, hyperstring,
                    missile, laser, railgun, torpedo, positron, destructor, gravitygun,
                    emsensor, gravsensor, qsensor, hsensor,
                    armour1, armour2, armour3, armour4, armour5, armour6, armour7,
                    engine1, engine2, engine3, engine4, engine5, engine6, engine7,
                    cryocell, pressurecell, nanocell, bioniccell, gravitycell, voretiocell,
                    inlinepod, externalpod, gravitypod,
      ]

      @all_tasks = [
        TaskTemplate.new("Разведчик", "", [sensors, fuel], [speed, cargo]),
        TaskTemplate.new("Боевой разведчик", "", [sensors, speed, fuel], [damage, armour]),
        TaskTemplate.new("Курьер", "", [speed], [fuel, cargo]),
        TaskTemplate.new("Танкер", "", [fuel], [speed]),
        TaskTemplate.new("Грузовой корабль", "", [cargo, speed], [fuel]),
        TaskTemplate.new("Пассажирский корабль", "", [fuel, cargo], [speed, sensors, armour]),
        TaskTemplate.new("Колонизатор", "", [cargo, armour, sensors], [damage, speed, fuel]),
        TaskTemplate.new("Станция слежения", "", [sensors], [] of Param),
        TaskTemplate.new("Инженерное судно", "", [cargo, energy], [fuel, speed]),

        TaskTemplate.new("Корабль сопровождения", "", [sensors, damage, armour], [speed, fuel]),
        TaskTemplate.new("Охотник на пиратов", "", [speed, damage, sensors], [armour, cargo]),
        TaskTemplate.new("Основа флота", "", [damage, armour, speed], [fuel]),
        TaskTemplate.new("Перехватчик", "", [speed, sensors], [damage]),
        TaskTemplate.new("Корабль прорыва", "", [armour, damage, speed], [speed]),
        TaskTemplate.new("Огневая платформа", "", [damage], [speed, cargo]),
        TaskTemplate.new("Боевой транспорт", "", [cargo, armour, speed], [damage]),
        TaskTemplate.new("Командный центр", "", [sensors, armour, cargo], [damage, speed]),

      ]
    end
  end
end
