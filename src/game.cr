require "./basic"
require "./project"
require "./demo"
require "./world"
require "./events"
require "./generation"
require "./order"

module Kbx
  class Game
    getter bureau
    getter enemies

    def initialize
      @setting = DemoSpace.new
      tasks = @setting.all_tasks.map(&.generate)
      @bureau = Bureau.new(@setting, BUREAU_NAMES[0], BUREAU_DESC[0], tasks)
      @enemies = Array(Bureau).new(BUREAU_NAMES.size - 1) { |i| Bureau.new(@setting, BUREAU_NAMES[i + 1], BUREAU_DESC[i + 1], tasks) }
      @bureau.log_news = false
      @enemies.each { |b| b.log_news = false }
      @bureau.fill_world_events(@enemies)
    end

    def reset
      @setting = DemoSpace.new
      tasks = @setting.all_tasks.map(&.generate)
      @bureau = Bureau.new(@setting, BUREAU_NAMES[0], BUREAU_DESC[0], tasks)
      @enemies = Array(Bureau).new(BUREAU_NAMES.size - 1) { |i| Bureau.new(@setting, BUREAU_NAMES[i + 1], BUREAU_DESC[i + 1], tasks) }
      @bureau.log_news = false
      @enemies.each { |b| b.log_news = false }
      @bureau.fill_world_events(@enemies)
    end

    def order(n, taskid, projid)
      return if n > @bureau.departments.size
      return if taskid > @bureau.production.size
      return if projid > @bureau.archive.size
      @bureau.departments[n - 1].order(Order.new(@bureau.production.keys[taskid - 1], projid > 0 ? @bureau.archive[projid - 1] : nil))
    end

    def day
      @bureau.update
      @enemies.each &.update
    end

    def week
      @bureau.update
      @enemies.each &.update
      6.times { @bureau.update(false); @enemies.each &.update(false) }
    end

    private def prod_for_json(bureau, task, proj)
      if proj
        {project: bureau.archive.index(proj), efficiency: (task.exprate(proj)).to_i, efficiency_rate: (task.percentrate(proj, false)*100).to_i, cost_rate: (task.percentrate(proj, true)*100).to_i}
      else
        {project: -1, efficiency: 0, efficiency_rate: 0, cost_rate: 0}
      end
    end

    private def prods_for_json(bureaus, task)
      list = bureaus.reject { |b| b.production[task].nil? }.map { |b| {b, b.production[task].not_nil!} }

      if list.size > 0
        b, proj = list.max_by { |b, proj| task.exprate(proj) }
        {name: "#{proj.name} (#{b.desc})", efficiency: (task.exprate(proj)).to_i, cost: proj.cost.to_i}
      else
        {name: "", efficiency: 0, cost: 1.0}
      end
    end

    def for_json
      {day:              @bureau.day,
       victory:          @bureau.victory,
       news:             @enemies.map(&.news_feed).flatten.select!(&.[](:important)) + @bureau.news_feed,
       tasks:            @bureau.production.keys.map(&.for_json),
       production:       @bureau.production.map { |task, proj| prod_for_json bureau, task, proj },
       archive:          @bureau.archive.map(&.for_json),
       enemy_production: @bureau.production.keys.map { |task| prods_for_json @enemies, task },
       departments:      @bureau.departments.map(&.for_json),
       nomenclature:     @bureau.nomenclature.map { |slot, list| {name: slot.name, list: list.map(&.for_json)} }}.to_json
    end
  end
end
