require "./game"

YEAR = 500

class TotalStats
  property count = 0
  property time_without_any_project = 0.0
  property time_search_100 = 0.0
  property time_search_end = 0.0
  property main_type = Hash(String, Float64).new(0.0)
  property percent_main = Hash(String, Float64).new(0.0)
  property final_tecs = Hash(String, Float64).new(0.0)
  property useless_tecs = Hash(String, Float64).new(0.0)
  property time_tech_develop_n = Hash(String, Int32).new(0)
  property time_tech_develop = Hash(String, Float64).new(0.0)
  property time_tech_develop_avg = 0.0
  property days_per_proto = 0.0
  property failed_protos = 0.0
  property useless_protos = 0.0
  property fit_protos = 0.0
  property bugs_effect = 0.0 # todo

  def divide
    @time_without_any_project /= count
    @time_search_100 /= count
    @time_search_end /= count
    @time_tech_develop.keys.each { |key| @time_tech_develop[key] /= time_tech_develop_n[key] }
    @time_tech_develop_avg = @time_tech_develop.values.sum / @time_tech_develop.values.size
    @days_per_proto /= count
    @failed_protos /= count
    @useless_protos /= count
    @fit_protos /= count
    @bugs_effect /= count
    @main_type.keys.each { |key| main_type[key] /= count }
    @percent_main.keys.each { |key| percent_main[key] /= count * main_type[key] }
    @final_tecs.keys.each { |key| final_tecs[key] /= count }
    @useless_tecs.keys.each { |key| useless_tecs[key] /= count * final_tecs[key] }
  end
end

STATS = TotalStats.new

module Kbx
  class BureauStats
    property days = 0
    property time_without_any_project = 0
    property time_search_100 = [] of Int32
    property time_search_end = [] of Int32
    property main_type = ""
    property percent_main = 0
    property final_tecs = [] of String
    property useless_tecs = [] of String
    property time_tech_develop = {} of String => Int32
    property finished_protos = 0
    property failed_protos = 0.0
    property useless_protos = 0.0
    property fit_protos = 0.0
    property bugs_effect = 0.0

    def add_to_stats
      STATS.count += 1
      STATS.time_without_any_project += @time_without_any_project / @days
      if time_search_100.size > 0
        STATS.time_search_100 += time_search_100.sum / time_search_100.size
      else
        STATS.time_search_100 += 100
      end
      if time_search_end.size > 0
        STATS.time_search_end += time_search_end.sum / time_search_end.size
      else
        STATS.time_search_end += 100
      end
      STATS.main_type[main_type] += 1
      STATS.percent_main[main_type] += percent_main
      final_tecs.each { |x| STATS.final_tecs[x] += 1 }
      useless_tecs.each { |x| STATS.useless_tecs[x] += 1 }
      time_tech_develop.each do |tech, n|
        STATS.time_tech_develop_n[tech] += 1
        STATS.time_tech_develop[tech] += n
      end
      STATS.days_per_proto += days / finished_protos
      STATS.failed_protos += failed_protos
      STATS.useless_protos += useless_protos
      STATS.fit_protos += fit_protos
      STATS.bugs_effect += bugs_effect
    end
  end

  class Bureau
    getter stats = BureauStats.new
    @search_started = StaticArray(Int32, N_DEPARTMENTS).new(-1)
    @tech_opened = {} of DeviceLine => Int32
    @tech_used = {} of DeviceLine => Int32

    def measure_day
      stats.days += 1
      stats.time_without_any_project += 1 if @departments.all? { |dep| dep.project.nil? }

      departments.each_with_index do |dep, i|
        if @search_started[i] < 0 && dep.project.nil?
          @search_started[i] = day
        elsif @search_started[i] >= 0 && !dep.project.nil?
          delta = day - @search_started[i]
          @search_started[i] = -1
          if day < 100
            stats.time_search_100 << delta
          elsif day > YEAR - 100
            stats.time_search_end << delta
          end
        end
      end

      @lines.each do |line|
        next if line.level == 1
        @tech_opened[line] = day unless @tech_opened[line]?
        @tech_used[line] = day if (!@tech_used[line]?) && production.values.reject(&.nil?).any? { |x| x.not_nil!.devices.any? { |d| d.line == line } }
      end
    end

    def final_stats
      types = @production.select { |key, value| value }.map { |key, value| value.not_nil!.chassis.line.name }.tally.max_by(&.[](1))
      stats.main_type = types[0]
      stats.percent_main = types[1]
      stats.final_tecs = @lines.map(&.name)
      stats.useless_tecs = @lines.reject do |line|
        early = line.level > 1 && @tech_opened[line] > YEAR - 100
        usefull = production.values.reject(&.nil?).any? { |x| x.not_nil!.devices.any? { |d| d.line == line } }
        early || usefull
      end.map(&.name)
      @tech_used.each { |line, use_day| stats.time_tech_develop[line.name] = use_day - @tech_opened[line] }
      stats.finished_protos = archive.count(&.complete?)
      stats.failed_protos = @failed_projects / (stats.finished_protos + @failed_projects)
      stats.useless_protos = (stats.finished_protos - archive.count(&.useful)) / stats.finished_protos
      stats.fit_protos = archive.count(&.wellfit) / stats.finished_protos
      stats.bugs_effect += @bugs_effect / N_DEPARTMENTS / @day
    end
  end

  class Game
    def day_stats
      @bureau.update
      @enemies.each &.update
      @bureau.measure_day
      @enemies.each &.measure_day
    end

    def final_stats
      @bureau.final_stats
      @enemies.each &.final_stats
      @bureau.stats.add_to_stats
      @enemies.each { |b| b.stats.add_to_stats }
    end
  end
end

20.times do |i|
  game = Kbx::Game.new
  puts "Universe #{i + 1} started"
  YEAR.times do |j|
    puts "Universe #{i + 1} day #{j + 1}" if j % 100 == 0
    game.day_stats
  end
  game.final_stats
end

STATS.divide
p STATS
