require "./basic"
require "./project"
require "./world"

module Kbx
  record Order, task : Task, base : Project?

  class Department
    def order(what : Order)
      return if what.task == @task && what.base == @project
      @enthusiasm = [@enthusiasm - 20, 20].max
      @task = what.task

      if proj = what.base
        if !proj.complete?
          if @parent.departments.any? { |x| x.project == proj && x != self }
            proj2 = @parent.duplicate_project proj
            proj2.initial_task = what.task
            @parent.name_for(proj2)
            @parent.archive << proj2
            @parent.news(name, "Проект #{proj.name} скопирован как #{proj2.name}")
            @project = proj2
          else
            @project = proj
          end
        else
          proj2 = @parent.rework_project proj, what.task, PROJ_REWORK_COUNT
          @parent.name_for(proj2)
          @parent.archive << proj2
          @parent.news(name, "Разработан эскиз: #{proj2.desc_for(what.task)} на базе серийного #{proj.name}")
          @project = proj2
        end
      else
        @project = nil
      end
      @parent.news(name, " Приказ по #{name}: разработать #{what.task.name} #{proj ? "взяв за основу #{proj.name}" : "создав новый проект"}")
    end
  end
end
